import { Injectable } from '@nestjs/common';
import { SubstrateService } from './substrate.service';

@Injectable()
export class AppService {
  constructor(readonly substrate: SubstrateService) {}

  async getChainData() {
    return this.substrate.contract();
  }
}
