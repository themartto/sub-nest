import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SubstrateService } from './substrate.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, SubstrateService],
})
export class AppModule {}
