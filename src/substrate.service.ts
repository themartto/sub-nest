import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { ApiPromise, Keyring, WsProvider } from '@polkadot/api';
import { Abi, ContractPromise } from '@polkadot/api-contract';

const SUBSTRATE_URL = 'ws://127.0.0.1:9944';
export const ERC20 = '5FffghG9xRA5gWsYWrKXz3MuBotNCe7p8dh1Eyse9caVotb5';

export const ACCOUNTS = {
  ALICE: {},
  BOB: {},
  CHARLIE: {},
  DAVE: {},
  EVE: {},
};

export class ChainData {
  chainType: string;
  nodeName: string;
  nodeVersion: string;
}

@Injectable()
export class SubstrateService implements OnModuleInit {
  api: ApiPromise;
  abi: Abi;
  flipper: ContractPromise;

  async onModuleInit() {
    Logger.log('Connecting to substrate chain...');
    const wsProvider = new WsProvider(SUBSTRATE_URL);
    this.api = await ApiPromise.create({
      provider: wsProvider,
    });

    await this.api.isReady;

    const keyring = new Keyring({ type: 'sr25519', ss58Format: 42 });
    ACCOUNTS.ALICE = keyring.addFromUri('//Alice', { name: 'Alice default' });
    ACCOUNTS.BOB = keyring.addFromUri('//Bob', { name: 'Bob default' });
    ACCOUNTS.CHARLIE = keyring.addFromUri('//Charlie', {
      name: 'Charlie default',
    });
    ACCOUNTS.DAVE = keyring.addFromUri('//Dave', { name: 'Dave default' });
    ACCOUNTS.EVE = keyring.addFromUri('//Eve', { name: 'Eve default' });

    this.abi = new Abi(
      require('./../contracts/example/target/ink/metadata.json'),
      this.api.registry.getChainProperties(),
    );

    this.flipper = await new ContractPromise(this.api, this.abi, ERC20);
  }

  async getChainData(): Promise<ChainData> {
    const chain = await this.api.rpc.system.chain();
    const nodeName = await this.api.rpc.system.name();
    const nodeVersion = await this.api.rpc.system.version();

    return {
      chainType: chain.toHuman(),
      nodeName: nodeName.toHuman(),
      nodeVersion: nodeVersion.toHuman(),
    } as ChainData;
  }

  async contract() {
    await this.flipper.tx.flip({ gasLimit: 0 }).signAndSend(
      // @ts-ignore
      ACCOUNTS.ALICE,
      { gasLimit: 0 },
      (r) => {
        if (r.status.isInBlock) {
          console.log(r.status.toHuman());
        }
      },
    );

    // we wait some time for the transaction to be completed
    setTimeout(async () => {
      // @ts-ignore
      const z = await this.flipper.query.get(ACCOUNTS.ALICE.address, {
        gasLimit: 0,
      });
      // @ts-ignore
      console.log(z.result.asOk.data.toHuman());
    }, 5000);
  }
}
